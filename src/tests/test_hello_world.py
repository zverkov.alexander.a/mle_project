from src.hello_world import sum_a_b


def test_sum_a_b():
    assert sum_a_b(3, 5) == 8
