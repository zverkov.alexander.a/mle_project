# Kaggle API
https://github.com/Kaggle/kaggle-api
conda install -c conda-forge kaggle 

touch ~/.kaggle/kaggle.json
( cp ~/Downloads/kaggle.json ~/.kaggle/kaggle.json ) 

chmod 600 ~/.kaggle/kaggle.json



kaggle competitions download -c ashrae-energy-prediction -p ./data/raw
unzip './data/raw/ashrae-energy-prediction.zip' -d './data/raw'
rm  './data/raw/ashrae-energy-prediction.zip'