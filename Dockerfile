FROM python:3.7

# set the working directory in the container
WORKDIR /app
COPY . .

# copy the dependencies file to the working directory
#COPY requirements.txt .

# install dependencies
#RUN virtualenv env
RUN python -m venv env
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH

RUN . env/bin/activate && pip install  -r  requirements.txt
RUN pip install jupyter

# command to run on container start
#CMD [ "python", "src/hello_world.py" ]
CMD jupyter notebook --allow-root --ip 0.0.0.0 --notebook-dir '/' --port 8999 --NotebookApp.token=''  --no-browser