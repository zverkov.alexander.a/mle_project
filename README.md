# 0. mle_project

Simple machine learning project
https://www.kaggle.com/c/ashrae-energy-prediction 

Gall’s Law: all complex systems that work evolved from simpler systems that worked. Complex systems are full of variables and Interdependencies that must be arranged just right in order to function. Complex systems designed from scratch will never work in the real world, since they haven’t been subject to environmental selection forces while being designed.

## 1. git
```
git init
git commit -m '123'
git push
git pull
```

add gitignore file
```
touch .gitignore
```
https://github.com/github/gitignore/blob/master/Python.gitignore

## 2. Structure
Take from: 
https://github.com/drivendata/cookiecutter-data-science 
```
├── LICENSE
├── Makefile           <- Makefile with commands like `make data` or `make train`
├── README.md          <- The top-level README for developers using this project.
├── data
│   ├── external       <- Data from third party sources.
│   ├── interim        <- Intermediate data that has been transformed.
│   ├── processed      <- The final, canonical data sets for modeling.
│   └── raw            <- The original, immutable data dump.
│
├── docs               <- A default Sphinx project; see sphinx-doc.org for details
│
├── models             <- Trained and serialized models, model predictions, or model summaries
│
├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
│                         the creator's initials, and a short `-` delimited description, e.g.
│                         `1.0-jqp-initial-data-exploration`.
│
├── references         <- Data dictionaries, manuals, and all other explanatory materials.
│
├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
│   └── figures        <- Generated graphics and figures to be used in reporting
│
├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
│                         generated with `pip freeze > requirements.txt`
│
├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
├── src                <- Source code for use in this project.
│   ├── __init__.py    <- Makes src a Python module
│   │
│   ├── data           <- Scripts to download or generate data
│   │   └── make_dataset.py
│   │
│   ├── features       <- Scripts to turn raw data into features for modeling
│   │   └── build_features.py
│   │
│   ├── models         <- Scripts to train models and then use trained models to make
│   │   │                 predictions
│   │   ├── predict_model.py
│   │   └── train_model.py
│   │
│   └── visualization  <- Scripts to create exploratory and results oriented visualizations
│       └── visualize.py
│

```

## 3. Python virtual environment   (https://realpython.com/python-virtual-environments-a-primer/)
```
$ python3 -m venv env 
$ source env/bin/activate 
$ deactivate 
```

```
$ which python
```

```
pip freeze > requirements.txt
```


 Run scripts
 ```
python src/hello_world.py 
python -m src.hello_world
```
## 4. Docker
```
touch Dockerfile 
touch .dockerignore
```

```
docker build -t mle .
docker run  -it --rm mle /bin/bash
```

Example : Kaggle docker https://github.com/Kaggle/docker-python 

#### docker without data 
```
docker run  -it --rm -p 127.0.0.1:8999:8999 mle  jupyter notebook --allow-root --ip 0.0.0.0 --notebook-dir '/' --port 8999
```

####  docker with data 
```
docker run  -it --rm -p 127.0.0.1:8999:8999 -v './data:/code/data' mle  jupyter notebook --allow-root --ip 0.0.0.0 --notebook-dir '/' --port 8999
```

```
 docker run -it --rm \
     -p   127.0.0.1:8999:8999   \
      -v '/Users/aleksandrzverkov/PycharmProjects/mle_project/data:/code/data' \
    --memory=10g \
    --cpus=4 \
    mle jupyter notebook --allow-root --ip 0.0.0.0 --notebook-dir '/' --port 8999
 ```

```    
CMD jupyter notebook --allow-root --ip 0.0.0.0 --notebook-dir '/' --port 8999 --NotebookApp.token=''  --no-browser
```
## 5. docker-compose
```
touch docker-compose.yaml
docker-compose build
docker-compose up
 ```
  
  
## Add tests
use pytest https://docs.pytest.org/en/6.2.x/goodpractices.html 

 add file  test_hello_world.py
 
``` 
pytest src
```

 
